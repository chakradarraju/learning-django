from polls.models import Poll, Choice
from django.template import RequestContext
from django.shortcuts import render_to_response as RR, get_object_or_404 as GO404
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse

def index(request):
    latest_poll_list = Poll.objects.all().order_by('-pub_date')[:5]
    return RR('polls/index.html', {'latest_poll_list':latest_poll_list})

def detail(request, poll_id):
    poll = GO404(Poll, pk=poll_id)
    return RR('polls/poll.html', {'poll':poll}, context_instance=RequestContext(request))

def results(request, poll_id):
    p = GO404(Poll, pk=poll_id)
    return RR('polls/results.html', {'poll': p})

def vote(request, poll_id):
    p = GO404(Poll, pk=poll_id)
    try:
        selected_choice = p.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return RR('polls/poll.html', {
            'poll': p,
            'error_message': "You didn't select a choice",
        }, context_instance=RequestContext(request))
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('polls.views.results', args=(p.id,)))
